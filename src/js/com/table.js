
 
var table = 
{  
    json:  "",
    campos: [],    
    html: "",
    linea:"",
    tbody_id:"",
    id:"",
    
    
    ini: function(obj) {        
        
        table.id = obj.tipo+ "-table";
        table.linea = obj.campoid;
        table.tbody_id = obj.tipo+"-tb";
        table.campos = obj.tablecampos;
        
    },
    
    
    
    
    get_html: function( ojson ) {
        
        //var oJson = JSON.parse(table.json) ;                    

        table.html = "";         
        

        for(x=0; x < ojson.length; x++) {     
    


            table.html += "<tr>";                  

            table.campos.forEach(function (elemento, indice, array) {                                

                //table.html += "<td>";     
/*
                try { 
                  table.html += "<td data-title=\""+table.etiquetas[indice]+"\">";                    
                }
                catch (e) {
                    table.html += "<td>";     
                }                
*/

console.log(elemento);

                table.html += "<td>";     
                
                                
                var jsonprop;


                if (Array.isArray( elemento )){
                    jsonprop = table.campos_array(elemento, oJson[x]);
                }
                else
                {                    
                    try {                        
                        eval("jsonprop = ojson[x]."+ elemento + ";");                            
                    }
                    catch(error) {                        
                        jsonprop = "";        
                    }
                }
                table.html += jsonprop;                     
                table.html += "</td>";



            });             
            table.html += "</tr>";    
        }


console.log(table.html );

        return table.html;
    },
    
    
    campos_array: function(miArray, json) {
        
        var ret = "";
        
        for (var i = 0; i < miArray.length; i+=1) {          
          eval("ret = ret +' '+json."+ miArray[i] + ";");              
        }        
        
        return ret;

    },    
    
    
    
    gene: function( ojson ) {
        document.getElementById( table.tbody_id ).innerHTML = table.get_html( ojson );  
    },


    
    limpiar: function() {     
        
        if (document.getElementById( table.tbody_id ))
        {
            table.json = '[]';
            table.gene();    
        }        
    },
        
        


    
    setObjeto: function( objeto) {
        
        table.id = objeto.tipo  + "-table";
        table.tbody_id =  objeto.tipo +"-tb";       
        table.campos = objeto.tablecampos;           
                       
    },
    
    set: {          
        tbodyid: function( objeto  ) {
            table.tbody_id = objeto.tipo+ "-tb";
        },
        
        tableid: function( objeto  ) {
            table.id = objeto.tipo+ "-table";
        },
    },




    
    refresh: function( obj, page, buscar, fn  ) {


        ajax.url = reflex.getApi( obj, page, buscar );

        ajax.metodo = "GET";        
        table.json = ajax.private.json();   
    
    
    
        table.campos = obj.tablecampos;                     
        table.set.tbodyid(obj);            
                
        table.gene();       
        table.formato(obj);
        table.lista_registro(obj, fn);        
        
        document.getElementById( obj.tipo + '_paginacion' ).innerHTML = paginacion.gene();      
        paginacion.move(obj, buscar, fn );          

    }, 



    
    refresh_promise: function( obj, page, buscar, fn  ) {


            let promesa = cereza.vista.lista_paginacion(obj, page);

            promesa        
                .then(( xhr ) => {         
                    cereza.html.url.redirect(xhr.status);                                                          
                    // botones de accion - nuevo para este caso

                })
                .catch(( xhr ) => { 

                    console.log(xhr.message);

                }); 




/*
        ajax.url = reflex.getApi( obj, page, buscar );

        ajax.metodo = "GET";        
        table.json = ajax.private.json();   
    
    
    
        table.campos = obj.tablecampos;                     
        table.set.tbodyid(obj);            
                
        table.gene();       
        table.formato(obj);
        table.lista_registro(obj, fn);        
        
        document.getElementById( obj.tipo + '_paginacion' ).innerHTML = paginacion.gene();      
        paginacion.move(obj, buscar, fn );          

*/

    }, 







    formato: function( obj ) {
        
              
        try { 
                   
                    
            var table = document.getElementById( table.id ).getElementsByTagName('tbody')[0];
            var rows = table.getElementsByTagName('tr');

            for (var i=0 ; i < rows.length; i++)
            {

                for (var j=0 ; j < obj.tableformat.length; j++)
                {

                    var type= obj.tableformat[j];
                    switch(type) {

                        case 'C':                                                 
                            break;                        

                        case 'N':                                  
                            
                            cell = table.rows[i].cells[j] ;                                  
                            cell.innerHTML = fmtNum(cell.innerHTML);                        
                            cell.style = "text-align: right";
                            break;

                        case 'D':                                                 
                            
                            cell = table.rows[i].cells[j] ;                                             
                            cell.innerHTML = fDMA (cell.innerHTML) ;                 
                            
                            cell.style = "text-align: right";
                            break;                        
                            
                        case 'R':                                                 
                            
                            cell = table.rows[i].cells[j] ;                               
                            cell.style = "text-align: right";
                            break;                        
                                          
                        
                        case 'Z':           
                            
                            cell = table.rows[i].cells[j] ;                                                 
                            
                            if (cell.innerHTML === 'undefined'){
                                cell.innerHTML = "";
                            }
                            else{
                                
                                cell.innerHTML = fmtNum(cell.innerHTML);                        
                                cell.style = "text-align: right";                                
                            }
                            break;                                                    
                            
                            
                        case 'U':           
                            
                            cell = table.rows[i].cells[j] ;                                                 
                            
                            if (cell.innerHTML === 'undefined'){
                                cell.innerHTML = "";
                            }
                            break;                                                    
                        
                        case 'E':           
                            
                            cell = table.rows[i].cells[j] ;                                
                            cell.innerHTML = "";
                            cell.style = "border: none";                                

                            break;l
    
                        case 'L':           
                            
                            cell = table.rows[i].cells[j] ;                 
                            
                            
                            if (cell.innerHTML.toString().trim() == 'null'){
                                cell.innerHTML = "";
                            }
                            else{
                                cell.innerHTML = fmtNum(cell.innerHTML);                        
                                cell.style = "text-align: right";
                            }
                            break;

                            
                            
                            break;                            
                        
                            
                        default:
                            //code block
                    }      
                }
            }
                    
        }
        catch (e) {  
        //objetoclase.funciones( obj, fn );                      
        }        
        
            
    },

    

    
    
};
 